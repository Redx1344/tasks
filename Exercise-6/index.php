<?php

// Connect to the database using MySQLi
$host = 'localhost';
$username = 'root';
$password = 'root';
$database = 'employee';


$conn = mysqli_connect($host, $username, $password, $database);

if(!$conn) {
    echo 'Connection error: ' . mysqli_connect_error();
}

# Insert data into employee table
$sql = "INSERT INTO employee (first_name, last_name, middle_name, birthday, address) VALUES ('Jane', 'Doe', 'Marie', '1990-01-01', '456 Elm Street')";

if ($conn->query($sql) === TRUE) {
    echo "<p> New record created successfully </p>";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

// retrieve the first name, last name, and birthday of all employees in the table
$sql =  "SELECT first_name, last_name, birthday FROM employee";
$result = $conn->query($sql);


if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"];
    }
} else {
    echo "0 results";
}

// // retrieve the number of employees whose last name starts with the letter 'S'
$sql =  "SELECT COUNT(*) AS num_of_s_last_name FROM employee WHERE last_name LIKE 'D%'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $count = $row["num_of_s_last_name"];
    echo "<p> Number of employees with last name starting with 'S': $count </p>";
} else {
    echo "No matching records found.";
}

//  retrieve the first name, last name, and address of the employee with the highest ID number
$sql =  "SELECT first_name, last_name, address FROM employee WHERE id = (SELECT MAX(id) FROM employee)";
$result = $conn->query($sql);


if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Address: " . $row["address"]. "<br>";
    }
} else {
    echo "0 results";
}

// Update data in the employee table
$sql =  "UPDATE employee SET address = '123 Main Street' WHERE first_name = 'Jane'";
if ($conn->query($sql) === TRUE) {
    echo "<p> Record updated successfully </p>";
} else {
    echo "Error updating record: " . $conn->error;
}

// retrieve the first name, last name, and birthday of 'Jane' after updating address
$sql =  "SELECT first_name, last_name, address FROM employee WHERE first_name='Jane'";
$result = $conn->query($sql);


if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - New Address: " . $row["address"];
    }
} else {
    echo "0 results";
}

// Delete data from the employee table
$sql = "DELETE FROM employee WHERE last_name LIKE 'D%'";
if ($conn->query($sql) === TRUE) {
    echo "<p> Record deleted successfully </p>";
} else {
    echo "Error deleting record: " . $conn->error;
}

$conn->close();

?>