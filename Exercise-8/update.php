<?php
include 'connect.php';
// Get the updateid passed in the URL and retrieve entry from the database associated with that id
if(isset($_GET['updateid'])) {
    $id = $_GET['updateid'];
    
    $db->where ("id", $id);
    $row = $db->getOne ("employee");

    $first_name = $row['first_name'];
    $middle_name = $row['middle_name'];
    $last_name = $row['last_name'];
    $birthday = $row['birthday'];
    $address = $row['address'];
}

// Update information once the form is submitted and redirect back to homepage
if (isset($_POST['submit'])) {
    $first_name = $_POST['first_name'];
    $middle_name = $_POST['middle_name'];
    $last_name = $_POST['last_name'];
    $birthday = $_POST['birthday'];
    $address = $_POST['address'];
    
    $new_data = Array ("first_name" => $first_name,
               "last_name" => $last_name,
               "middle_name" => $middle_name,
               "birthday" => $birthday,
               "address" => $address,
    );

    $db->where ('id', $id);
    if ($db->update ('employee', $new_data))
        header('location:index.php');
    else
        echo 'update failed: ' . $db->getLastError();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <title>Update Employee Information</title>
</head>
<body>
   <div class="container-lg">
        <div class="text-center">
            <h2>Edit Information</h2>
        </div>

        <div class="row justify-content-center my-5">
            <div class="col-lg-6">
                <form method="post">
                    <div class="form-group">
                        <label for="name" class="form-label">Full Name:</label>
                        <div class="row">
                            <div class="mb-4 input-group">
                                <span class="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" 
                                    fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                                        <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 
                                        1H3Zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Z"/>
                                    </svg>
                                </span>
                                <div class="col-4">
                                    <input type="text" class="form-control" id="name" placeholder="First Name"
                                    value=<?php echo $first_name?> name="first_name">
                                </div>

                                <div class="col-3">
                                    <input type="text" class="form-control" id="name" placeholder="Middle Name"
                                    value=<?php echo $middle_name?>  name="middle_name">
                                </div>

                                <div class="col-4">
                                    <input type="text" class="form-control" id="name" placeholder="Last Name"
                                    value=<?php echo $last_name?>  name="last_name">
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="birthday" class="form-label">Birthdate:</label>
                        <div class="mb-4 input-group">
                            <span class="input-group-text">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-event-fill" viewBox="0 0 16 16">
                                <path d="M4 .5a.5.5 0 0 0-1 0V1H2a2 2 0 0 0-2 2v1h16V3a2 2 0 0 0-2-2h-1V.5a.5.5 0 0 0-1 0V1H4V.5zM16 14V5H0v9a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2zm-3.5-7h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5z"/>
                                </svg>
                            </span>
                            <input type="text" class="form-control" id="birthday" name="birthday" 
                            value=<?php echo $birthday?> placeholder="YYYY-MM-DD">
                        </div>
                    </div>
                    
                    
                    
                    <div class="form-group">
                        <label for="address" class="form-label">Address:</label>
                        <div class="mb-4 input-group">
                            <span class="input-group-text">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-house-fill" viewBox="0 0 16 16">
                                <path d="M8.707 1.5a1 1 0 0 0-1.414 0L.646 8.146a.5.5 0 0 0 .708.708L8 2.207l6.646 6.647a.5.5 0 0 0 .708-.708L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.707 1.5Z"/>
                                <path d="m8 3.293 6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6Z"/>
                                </svg>
                            </span>
                            <input type="text" class="form-control" id="address" placeholder="e.g. 40 Some Street" 
                            value='<?php echo $address?>' name="address">
                        </div>
                    </div>
                    

                    <div class="mb-4 text-center">
                        <input type="submit" value="Update" name="submit" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>
</html>