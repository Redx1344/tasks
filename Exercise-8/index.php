<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <title>Employee Management System</title>
</head>
<body class="bg-dark">
   
    <div class="container-lg">
        <div class="container d-flex justify-content-end">
            <a class="btn btn-success my-4" 
            href="create.php"><svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 640 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M96 128a128 128 0 1 1 256 0A128 128 0 1 1 96 128zM0 482.3C0 383.8 79.8 304 178.3 304h91.4C368.2 304 448 383.8 448 482.3c0 16.4-13.3 29.7-29.7 29.7H29.7C13.3 512 0 498.7 0 482.3zM504 312V248H440c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V136c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H552v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z"/></svg> Add User</a>
        </div>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header text-center">
                        <h2 class="display-6">Employee Table</h2>
                    </div>

                    <div class="card-body">
                        <table class="table table-bordered table-hover text-center">
                            <thead class="table-dark text-white">
                                <tr>
                                    <th scope="col">Employee ID</th>
                                    <th scope="col">First Name</th>
                                    <th scope="col">Middle Name</th>
                                    <th scope="col">Last Name</th>
                                    <th scope="col">Birthday</th>
                                    <th scope="col">Address</th>
                                    <th scope="col">Edit</th>
                                    <th scope="col">Delete</th>
                                </tr>
                            </thead>

                            <?php 
                            include 'connect.php';
                            $result = $db->get('employee');
                            if ($db->count > 0) {
                                foreach ($result as $row) {
                                    $id = $row['id'];
                                    echo '<tr> 
                                    <th scope="row">' .$id. '</th>
                                    <td>' .$row['first_name'].'</td>
                                    <td>' .$row['middle_name'].'</td>
                                    <td>' .$row['last_name'].'</td>
                                    <td>' .$row['birthday'].'</td>
                                    <td>' .$row['address'].'</td>
                                    <td>
                                        <a href="update.php?updateid='.$id.'" class="btn btn-primary">Edit</a>
                                    </td>
                                    <td>
                                        <a href="delete.php?deleteid='.$id.'" class="btn btn-danger">Delete</a>
                                    </td>
                                    </tr>';
                                }
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

 
</body>
</html>