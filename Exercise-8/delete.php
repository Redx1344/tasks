<?php
// Delete employee from the database associated with the id fetched from the URL 
include 'connect.php';
if(isset($_GET['deleteid'])) {
    $id = $_GET['deleteid'];

    $sql = $db->where('id',$id);
    if($db->delete('employee')) header('location:index.php');

}

?>