<?php 
  /**
   * Tells the browser to allow code from any origin to access
   */
  header("Access-Control-Allow-Origin: *");


  /**
   * Tells browsers whether to expose the response to the frontend JavaScript code
   * when the request's credentials mode (Request.credentials) is include
   */
  header("Access-Control-Allow-Credentials: true");
 


  /**
   * Specifies one or more methods allowed when accessing a resource in response to a preflight request
   */
  header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
 
  /**
   * Used in response to a preflight request which includes the Access-Control-Request-Headers to
   * indicate which HTTP headers can be used during the actual request
   */
  header("Access-Control-Allow-Headers: Content-Type");

require_once('MysqliDb.php');

class API {
    private $db;

    public function __construct()
    {
        // NOTE: Database here is named "users" because the name "employee" already exist in my phpMyAdmin and is used by the previous exercises.
        $this->db = new MysqliDB('localhost', 'root', 'root', 'users');
    }

    public function httpGet($payload)
    {
        // check if the payload or parameters is an array
        if (!is_array($payload)) {
            return $this->response('GET', 'failed', 'Payload must be an array');
        }

        // Fetch all records from a table
        $result = $this->db->get('information');

        if($result) {
            return $this->response('GET', 'success', $result);
        } else {
            return $this->response('GET', 'failed', 'Failed to fetch data');
        }
    }
    
    public function httpPost($payload)
    {
        // check if the payload is an array and if it is not empty. 
        if (!is_array($payload) || empty($payload)) {
            return $this->response('POST', 'failed', 'Payload must be a non-empty array');
        }

        // If the payload is an array of records, insert them all
        if (isset($payload[0])) {
            $insertedRecords = array();
            foreach ($payload as $record) {
                $data = $this->prepareData($record);
                $result = $this->db->insert('information', $data);

                if ($result) {
                    $insertedRecords[] = $data;
                }
            }

            if (!empty($insertedRecords)) {
                return $this->response('POST', 'success', $insertedRecords, 'Records updated successfully');
            } else {
                return $this->response('POST', 'failed', 'Failed to insert data');
            }
            
        } else {
            // If the payload is a single record, Insert record in a table
            $data = $this->prepareData($payload);
            $result = $this->db->insert('information', $data);

            if($result){
                return $this->response('POST', 'success', $data, 'Record inserted successfully');
            } else {
                return $this->response('POST', 'failed', 'Failed to insert data');
            }
        }
    }

    public function httpPut($id, $payload)
    {
        // check if id is not null or empty and check if $payload is not empty
        if (empty($id) || !is_array($payload) || empty($payload)) {
            return $this->response('PUT', 'failed', 'Invalid Input Parameters');
        }

        // Check if the ID in the payload matches the provided ID
        if ($payload['id'] != $id) {
            return $this->response('PUT', 'failed', 'ID mismatch');
        }

        // Update a record in a table
        $data = $this->prepareData($payload);
        $this->db->where('id', $id);
        $result = $this->db->update('information', $data);

        if ($result) {
            return $this->response('PUT', 'success', $data, 'Record updated successfully');
        } else {
            return $this->response('PUT', 'failed', 'Failed to Update Data');
        }
    }

    public function httpDelete($id, $payload)
    {
        // Check if id or payload is not null or empty
        if (empty($id) || empty($payload)) {
            return $this->response('DELETE', 'failed', 'Invalid Input Parameters');
        }

        // Check if payload['id'] is an array or not
         // If the payload['id'] is not an array, use the where() function from the MysqliDB class, specify the ID, and use '$id' in the WHERE clause condition. Otherwise, do the same steps but include the SQL 'IN' operator.

        if (!is_array($payload['id'])) {
            // Check if the ID in the payload matches the provided ID
            if ($payload['id'] != $id ){
                    return $this->response('DELETE', 'failed', 'ID mismatch');
                }
            $this->db->where('id', $id);
          
        } else {
            // Check if the ID in the payload matches the provided ID
            // If one of the ids do not match, the DELETE function would not proceed

            for ($i = 0; $i < count($payload['id']); $i++) {
                if ($payload['id'][$i] != $id[$i]) {
                    return $this->response('DELETE', 'failed', 'ID(s) mismatch');
                }
            }
            $idsToDelete = $payload['id'];
            $this->db->where('id', $idsToDelete, 'IN');
        }

        // Delete a record from a table
        $result = $this->db->delete('information');

        if ($result) {
            return $this->response('DELETE', 'success', $result, 'Deleted Succesfully');
        } else {
            return $this->response('DELETE', 'failed', 'Record not found or could not be deleted');
        }
    }

    private function prepareData($payload)
    {
        $data = array(
            'first_name' => $payload['first_name'],
            'middle_name' => $payload['middle_name'],
            'last_name' => $payload['last_name'],
            'contact_number' => $payload['contact_number'],
        );

        if (array_key_exists('id', $payload)) {
            $data['id'] = $payload['id'];
        }

        return $data;
    }

     private function response($method, $status, $data, $message = null)
    {
        $response = array(
            'method' => $method,
            'status' => $status,
            'data' => $data,
        );

        if (!is_null($message)) {
            $response['message'] = $message;
        }

        return json_encode($response);
    }
}

// Comment out the identifier of request until the last line of code



// Get the request method or set a default value
$request_method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'UNKNOWN';

if ($request_method === 'GET') {
    $received_data = $_GET;
} else {
    // Extract the IDs for PUT and DELETE requests
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
        $request_uri = $_SERVER['REQUEST_URI'];
        $exploded_request_uri = array_values(explode("/", $request_uri));
        $last_index = count($exploded_request_uri) - 1;

        // Check if the last segment contains a comma (indicating multiple IDs)
        if (strpos($exploded_request_uri[$last_index], ',') !== false) {
            // Split the comma-separated IDs into an array
            $ids = explode(',', $exploded_request_uri[$last_index]);
        } else {
            $ids = $exploded_request_uri[$last_index];
        }  
    }

    // Extract payload data for POST, PUT, and DELETE requests
    $received_data = json_decode(file_get_contents('php://input'), true);
}

$api = new API;

switch ($request_method) {
    case 'GET':
        $api->httpGet($received_data);
        break;
    case 'POST':
        $api->httpPost($received_data);
        break;
    case 'PUT':
        $api->httpPut($ids, $received_data);
        break;
    case 'DELETE':
        $api->httpDelete($ids, $received_data);
        break;
    default:
        return json_encode(array(
            'method' => $request_method,
            'status' => 'failed',
            'message' => 'Unknown request method',
        ));
}


?>