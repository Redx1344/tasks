<?php

require_once 'API.php';
use PHPUnit\Framework\TestCase;


class APITest extends TestCase
{
    private $api;

    protected function setUp(): void
    {
      $this->api = new API();
    }

    /**
     * @covers APITest::testhttpGet
     */
    
    public function testHttpGet()
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';

        $payload = array();

        // Perform the test
        $result = json_decode($this->api->httpGet($payload), true);

        // Assert the results
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result); // Check if 'data' key exists
        $this->assertNotEmpty($result['data']); // Check if 'data' is not empty

        // Assert that specific keys exist in the 'payload'
        $expectedKeys = [
            'method',
            'status',
            'data',
        ];

        $actualKeysInPayload = array_keys($result);
        $this->assertEquals($expectedKeys, $actualKeysInPayload);  
    
        # To avoid affecting the other tests
        unset($_SERVER['REQUEST_METHOD']);
    }
    
    /**
     * @covers APITest::testhttpPost
     */
    
    public function testHttpPost()
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';

        $id = 35;

        $payload = array(
            'id' => $id,
            'first_name' => 'Test',
            'middle_name' => 'test',
            'last_name' => 'last test',
            'contact_number' => 654655
        );

        // Perform the test
        $result = json_decode($this->api->httpPost($payload), true);
    
        // Assert the results
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result); // Check if 'data' key exists
        $this->assertNotEmpty($result['data']); // Check if 'data' is not empty

        // Assert that specific keys exist in the 'data' array
        $expectedKeys = [
            'id',
            'first_name',
            'middle_name',
            'last_name',
            'contact_number'
        ];
        $actualKeysInPayload = array_keys($payload);
        $this->assertEquals($expectedKeys, $actualKeysInPayload);  

        # To avoid affecting the other tests
        unset($_SERVER['REQUEST_METHOD']);
    }

    public function testHttpPut()
    {
        
        $_SERVER['REQUEST_METHOD'] = 'PUT';

        $id = 35;

        $payload = array(
            'id' => $id,
            'first_name' => 'Test',
            'middle_name' => 'test',
            'last_name' => 'latest test',
            'contact_number' => 654655
        );

        // Perform the test
        $result = json_decode($this->api->httpPut($id,$payload), true);
       
        // Assert the results
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
        $this->assertNotEmpty($result['data']); // Check if 'data' is not empty

        // Assert that specific keys exist in the 'data' array
        $expectedKeys = [
            'id',
            'first_name',
            'middle_name',
            'last_name',
            'contact_number'
        ];
        $actualKeysInPayload = array_keys($payload);
        $this->assertEquals($expectedKeys, $actualKeysInPayload);  

        # To avoid affecting the other tests
        unset($_SERVER['REQUEST_METHOD']);
    }

    public function testHttpDelete()
    {
        $_SERVER['REQUEST_METHOD'] = 'DELETE';
        
        // Put in any existing record's id in this array
        $id = 35;
        $idsToDelete = [];

        $payload = array(
            'id' => $id
        );

        
        // Perform the test
        $result = json_decode($this->api->httpDelete($id,$payload), true);

        // Assert the results
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
        $this->assertNotEmpty($result['data']); // Check if 'data' is not empty

        // Assert that specific keys exist in the 'data' array
        $expectedKeys = [
            'id',
        ];

        // If I used $result here, it will return a boolean value which is not what we want. We want to check if the payload only contains the expected keys. 

        $actualKeysInPayload = array_keys($payload);
        $this->assertEquals($expectedKeys, $actualKeysInPayload);

        # To avoid affecting the other tests
       unset($_SERVER['REQUEST_METHOD']);
    }
}


