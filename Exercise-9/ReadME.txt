For POST requests, JSON has to be in this format:

    {
        "first_name": // First name of employee,
        "middle_name": // Middle name of employee,
        "last_name": // Last Name of employee,
        "contact_number": // Contact number of employee
    }

or if you want to specify the id, it could be in this format: 

    {
        "id" : // id of employee 
        "first_name": // First name of employee,
        "middle_name": // Middle name of employee,
        "last_name": // Last Name of employee,
        "contact_number": // Contact number of employee
    }

Now, if you want to put multiple inputs all at once, you could give an array of objects to the payload like this: 

    [
        {
            "first_name": "Michael",
            "middle_name": "A",
            "last_name": "Johnson",
            "contact_number": "111222333"
        },
        {
            "first_name": "Emily",
            "middle_name": "B",
            "last_name": "Smith",
            "contact_number": "444555666"
        },
        {
            "first_name": "Daniel",
            "middle_name": "C",
            "last_name": "Williams",
            "contact_number": "777888999"
        }
    ]


For PUT requests, you have to make sure that <id> in the endpoint is the same as in the payload:

Example (endpoint): http://localhost/api.php/<id> 

Example (payload): 

    {
        "id": <id>,
        "first_name": // First name of employee,
        "middle_name": // Middle name of employee,
        "last_name": // Last Name of employee,
        "contact_number": // Contact number of employee
    }



For DELETE requests, you have to make sure that <id> in the endpoint is the same as in the payload.

Example (endpoint): http://localhost/api.php/<id> 

Example (payload): 

    {
        "id": <id>,
        "first_name": // First name of employee,
        "middle_name": // Middle name of employee,
        "last_name": // Last Name of employee,
        "contact_number": // Contact number of employee
    }

For deleting multiple data, we just need to pass an array of id in the payload as well as comma separated id in the endpoint. 

Example (endpoint): http://localhost/api.php/<id1>,<id2>...

Example (payload): 

    {
        "id": [<id1>,<id2>]
        
    }



For Unit Testing, first we have to `cd vendor/bin` and then we type this command in the CLI: 

`.\phpunit --configuration ../../phpunit.xml` to run the tests.

This assumes that you used composer to download PHPUnit and the bootstrap attribute in the `phpunit.xml` file looks like this:

```
    <phpunit bootstrap="vendor/autoload.php" colors="true" verbose="true" stderr="true" stopOnFailure="false">
```