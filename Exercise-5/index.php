<?php
if ($_SERVER["REQUEST_METHOD"] === "POST") {
        if (isset($_POST["name"])) {
            $emoji = array_rand(array("👋"=>"1", "🔥"=>"2", "😎"=>"3", "🚀"=>"4"));
            $userName = $_POST["name"];
            echo "<header><h1>Hello, $userName! Welcome to our website.$emoji</h1></header>";
        }
    }
?>

<!DOCTYPE html>
<html lang="en" data-theme="dark">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@picocss/pico@1/css/pico.min.css">
  <title>Document</title>
  
</head>
<body>
  <form method="POST">
    <label for="name">Enter your name:</label>
    <input type="text" id="name" name="name">
    <button type="submit">Submit</button>
  </form>
   
  <style>
    body {
      padding: 2rem;
    }

    p {
      color: white;
      font-weight: bold;
    }

    header {
      display: flex;
      background-image: url(space.jpg);
      justify-content: center;
    }

  </style>
</body>
</html>

