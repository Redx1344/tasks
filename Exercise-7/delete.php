<?php
// Delete employee from the database associated with the id fetched from the URL 
include 'connect.php';
if(isset($_GET['deleteid'])) {
    $id = $_GET['deleteid'];

    $sql = "DELETE FROM employee where id=$id";

    if ($conn->query($sql) === TRUE) {
        header('location:index.php');
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

?>